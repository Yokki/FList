//
//  FList.cpp
//  FList
//
//  Created by Игорь on 11.03.18.
//  Copyright © 2018 Игорь. All rights reserved.
//

#include "FList.hpp"

struct Node;

auto FList::insert(int val) -> void
{

    if (first == nullptr) {
        //yell_error();
        std::cout << "error";
        return;
    }
    Node* working = first;
    while (working->next != nullptr) {
        working = working->next;
    }
    working->next = new Node{ val, nullptr };
}

auto FList::remove(int val) -> bool
{
    if (first == nullptr) {
        //yell_error();
        std::cout << "error";
        return 0;
    }
    Node* working = first;
    if (first->data == val) {
        //yell_error();
        Node* temp = first;
        first = first->next;
        delete temp;
        return 0;
    }
    while (working != nullptr) {
        if (working->next->data == val) {
            delete working->next;
            working->next = nullptr;
            return 1;
        }
        working = working->next;
    }
    return 0;
}

const void FList::print()
{
    if (first == nullptr) {
        std::cout << "список пуст" << std::endl;
        return;
    }
    Node* working = first;
    while (working->next != nullptr) {
        std::cout << working->data << " -> ";
        working = working->next;
    }
    std::cout << working->data << std::endl;
}

auto FList::addfirst(Node* node_) -> void
{
    first = node_;
}
FList::FList(Node* node_)
{
    first = node_; // this -> {ptr = "nullptr", size = 0}
}
FList::FList()
{
    first = nullptr; // this -> {ptr = "nullptr", size = 0}
}
FList::~FList()
{
    for (Node* temp = first; temp != nullptr; temp = temp->next) {
        delete temp;
    }
}
