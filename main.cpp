//
//  main.cpp
//  FList
//
//  Created by Игорь on 11.03.18.
//  Copyright © 2018 Игорь. All rights reserved.
//

#include "FList.hpp"
#include <locale.h>

void tui();
void case1(FList);
void case7();

int main(int argc, char* argv[])
{

    ///
    FList list{ nullptr };
    ///
    setlocale(LC_ALL, "rus");
    tui();

    if (argc < 2) {
        // empty
    }
    else if (argc == 2) {
        // через запятую
        list.addfirst(new Node{ atoi(argv[1]), nullptr });

        char* input = argv[1];
        for (int i = 0;; i++) {
            input = strchr(input, ',') + 1;
            list.insert(atoi(input));
            if (strchr(input, ',') == 0)
                break;
        }
    }
    else {
        // через пробел
        list.addfirst(new Node{ atoi(argv[1]), nullptr });
        for (int i = 2; i < argc; i++) {
            list.insert(atoi(argv[i]));
        }
    }
    int input;
    while (true) {
        while (!(std::cin >> input) || input < 1 || input > 7) {
            std::cin.clear();
            //cin.get();
            //yell_error();
            //yell_sellect();
        }
        switch (input) {
        case 1:
            case1(list);
            break;
        case 7:
            case7();
            break;
        }
    }
}
void tui()
{
    std::cout << "Выберите одну из операций: " << std::endl;
    std::cout << "1. Распечатать список " << std::endl;
    std::cout << "2. Добавить элементы в список" << std::endl;
    std::cout << "3. Удалить элемент" << std::endl;
    std::cout << "4. Найти позиции элементов" << std::endl;
    std::cout << "5. Заменить элемент на другой" << std::endl;
    std::cout << "6. Отсортировать элементы списка" << std::endl;
    std::cout << "7. Завершить работу программы" << std::endl;
}
void case1(FList list)
{
    list.print();
}
void case7()
{
    std::cout << "Вы   хотите   выйти   из   программы   ?   (Y/N):";
    std::string input;
    std::cin >> input;
    if ((input == "y") or (input == "yes") or (input == "Yes")
        or (input == "Y") or (input == "YES")) {
        std::cout << "До свидания!" << std::endl;
        exit(0);
    }
}
