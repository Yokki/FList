//
//  FList.hpp
//  FList
//
//  Created by Игорь on 11.03.18.
//  Copyright © 2018 Игорь. All rights reserved.
//

#ifndef FList_hpp
#define FList_hpp
#include <iostream>
#include <stdio.h>

#endif /* FList_hpp */
struct Node {
    int data;
    Node* next;
};
//void yell_error() { std::cout << "error";}
class FList {
    Node* first;

public:
    auto insert(int val) -> void;
    auto remove(int val) -> bool;
    const void print();
    auto addfirst(Node* node_) -> void;
    FList(Node* node_);
    FList();
    ~FList();
};
